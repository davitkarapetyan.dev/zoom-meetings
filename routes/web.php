<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/meetings', 'MeetingsController@index')->name('meetings');
Route::get('/meetings/add', 'MeetingsController@add')->name('meetings-add');
Route::post('/meetings/add', 'MeetingsController@add')->name('meetings-add_post');