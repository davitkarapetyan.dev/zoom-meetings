<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Teacher role
        $teacher = new Role();
        $teacher->name = "teacher";
        $teacher->description = "Teacher";
        $teacher->save();

        //Student role
        $teacher = new Role();
        $teacher->name = "student";
        $teacher->description = "Student";
        $teacher->save();

        $this->command->info('Roles table seeded!');
    }
}
