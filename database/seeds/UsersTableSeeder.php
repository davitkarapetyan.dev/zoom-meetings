<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Teacher
        $role_teacher = Role::where("name", "teacher")->first(['id']);
        if (!empty($role_teacher)) {
            $user_teacher = new User();
            $user_teacher->role_id = $role_teacher->id;
            $user_teacher->name = "Teacher 1";
            $user_teacher->password = bcrypt(env('DEFAULT_PASSWORD', '123456'));
            $user_teacher->email = "techer1@zoom-meetings.loc";
            $user_teacher->save();
        }

        // Student
        $role_student = Role::where("name", "student")->first(['id']);
        if (!empty($role_student)) {
            $user_student = new User();
            $user_student->role_id = $role_student->id;
            $user_student->name = "Student 1";
            $user_student->password = bcrypt(env('DEFAULT_PASSWORD', '123456'));
            $user_student->email = "student1@zoom-meetings.loc";
            $user_student->save();
        }
        $this->command->info('Users table seeded!');
    }
}
