<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class HomeController extends BaseSchoolController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loggedUser = Auth::user();
        $role = null;
        $loggedUserRole = $loggedUser->role;
        if (!empty($loggedUserRole)) {
            $role = $loggedUserRole->name;
        }

        return view('home', ['role' => $role]);
    }
}
