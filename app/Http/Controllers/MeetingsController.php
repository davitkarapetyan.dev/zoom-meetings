<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class MeetingsController extends BaseSchoolController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new Client();
        $meetings_url = config('app.zoom.endpoint') . 'users/' . config('app.zoom.user_id') . '/meetings';
        $response = $client->get($meetings_url, [
            'body' => json_encode(['type' => 'scheduled']),
            'headers' => [
                'Authorization' => "Bearer " . $this->jwt_token,
            ]
        ])->getBody();

        $loggedUser = Auth::user();
        $role = null;
        $loggedUserRole = $loggedUser->role;
        if (!empty($loggedUserRole)) {
            $role = $loggedUserRole->name;
        }

        return view('meetings.index', ['data' => json_decode($response), 'role' => $role]);
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'topic' => 'required|string|max:255'
            ]);

            // Validation is ok

            // Creating meetings
            $meeting = new \stdClass();
            $meeting->topic = $request->topic;
            /*
                Meeting Type
                1 Instant Meeting
                2 Scheduled Meeting
                3 Recurring Meeting with no fixed time
                8 Recurring Meeting with fixed time
            */
            $meeting->type = 3; // example

            $meetings_url = config('app.zoom.endpoint') . 'users/' . config('app.zoom.user_id') . '/meetings';

            $client = new Client();
            $request = $client->post($meetings_url, [
                'body' => json_encode($meeting),
                'headers' => [
                    'Authorization' => "Bearer " . $this->jwt_token,
                    'Content-Type' => "application/json",
                ]
            ]);
            return redirect('meetings')->with('status', 'Added new meeting');
        }


       return view('meetings.add', []);
    }
}
