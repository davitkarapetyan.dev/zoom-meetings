<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

// JWT PHP Library https://github.com/firebase/php-jwt
use Firebase\JWT\JWT;

class BaseSchoolController extends Controller
{

    public $jwt_token;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->generateJWT();


    }

    // function to generate JWT
    public function generateJWT()
    {
        $this->jwt_token = session()->get('jwt_token');
        if (empty($this->jwt_token)) {
            //Zoom API credentials from https://developer.zoom.us/me/
            $key = config('app.zoom.api_key');
            $secret = config('app.zoom.api_secret');
            $token = array(
                "iss" => $key,
                // The benefit of JWT is expiry tokens, we'll set this one to expire in 120 minutes
                "exp" => time() + 120 * 60
            );
            $this->jwt_token = JWT::encode($token, $secret);
            session(['jwt_token' => $this->jwt_token]);
        }
        return $this->jwt_token;
    }
}
