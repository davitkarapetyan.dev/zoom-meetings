@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Meetings</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($role === "teacher")
                        <div style="float:right; margin-bottom: 20px">
                            <a href="{{ route('meetings-add') }}">+Add Meeting</a>
                        </div>
                    @endif

                    <div>
                        <table id="meetings" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th>Meeting ID</th>
                                <th>Topic</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data->meetings as $meeting)
                                <tr>
                                    <td>{{$meeting->id}}</td>
                                    <td>{{$meeting->topic}}</td>
                                    <td><a role="button" href="{{ $meeting->join_url }}" class="btn btn-default btn-sm" target="_blank">Start</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script src="js/meetings.js"></script>
@endpush

@endsection
