## Zoom Meetings

**Requirements:** php ^7.1.3, composer, php7.1-curl, etc ...

**Commands:**

- git clone https://gitlab.com/davitkarapetyan.dev/zoom-meetings.git
- cd zoom-meetings
- composer install
- if .env file doesn't exist copy .env.example to .env
- IMPORTANT: change **.env** file DB_DATABASE, DB_USERNAME and DB_PASSWORD  (database charset utf8mb4_unicode_ci)

    Example:    CREATE DATABASE zoom_meetings CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
    
- php artisan key:generate
- php artisan migrate --seed
- php artisan serve

** Laravel development server started: **http://127.0.0.1:8000/**

Users can login here http://127.0.0.1:8000/login

    Teacher: techer1@zoom-meetings.loc / 123456
    Student: student1@zoom-meetings.loc / 123456
    
Registration here http://127.0.0.1:8000/register

After success login depends on role users can see below pages:

- http://127.0.0.1:8000/home
- http://127.0.0.1:8000/meetings
- http://127.0.0.1:8000/meetings/add